﻿using System.Collections.Generic;
using WebShop.Data.Interfaces;

namespace WebShop.Data.Models{
	public class Manufacturer : IEntity{
		public int Id{ get; set; }
		public string Name{ get; set; }
		public List<Product> Products{ get; set; }
	}
}