﻿using System.Collections.Generic;
using WebShop.Data.Interfaces;

namespace WebShop.Data.Models{
	public class Category : IEntity{
		public int Id{ get; set; }
		public string Name{ get; set; }
		public int ParentCategoryId{ get; set; }
		public Category ParentCategory{ get; set; }
		public List<CategoryProduct> CategoryProducts{ get; set; }
	}
}