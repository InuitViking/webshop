﻿using WebShop.Data.Interfaces;

namespace WebShop.Data.Models{
	public class CategoryProduct : IEntity{
		public int Id{ get; set; }
		public int CategoryId{ get; set; }
		public Category Category{ get; set; }
		public int ProductId{ get; set; }
		public Product Product{ get; set; }
	}
}