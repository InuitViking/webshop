﻿using System.Collections.Generic;
using WebShop.Data.Interfaces;

namespace WebShop.Data.Models{
	public class Customer : IEntity{
		public int Id{ get; set; }
		#nullable enable
		public string? Name{ get; set; }
		#nullable disable
		public string Email{ get; set; }
		public string Password{ get; set; }
		public List<Review> Reviews{ get; set; }
	}
}