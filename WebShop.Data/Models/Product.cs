﻿using System;
using System.Collections.Generic;
using WebShop.Data.Interfaces;

namespace WebShop.Data.Models{
	public class Product : IEntity{
		public int Id{ get; set; }
		public string Name{ get; set; }
		public string Description{ get; set; }
		public decimal Price{ get; set; }
		public int Amount{ get; set; }
		public string Image{ get; set; }
		public DateTime CreatedOn{ get; set; }
		public DateTime PublishedOn{ get; set; }
		public int SoftDeleted{ get; set; }
		public int ManufacturerId{ get; set; }
		public Manufacturer Manufacturer{ get; set; }
		public List<CategoryProduct> CategoryProducts{ get; set; }
		public List<Review> Reviews{ get; set; }
	}
}