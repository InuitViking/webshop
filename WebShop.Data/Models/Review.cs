﻿using WebShop.Data.Interfaces;

namespace WebShop.Data.Models{
	public class Review : IEntity{
		public int Id{ get; set; }
		public int Rating{ get; set; }
		public string Text{ get; set; }
		public int ProductId{ get; set; }
		public Product Product{ get; set; }
		public int CustomerId{ get; set; }
		public Customer Customer{ get; set; }
	}
}