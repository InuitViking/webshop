﻿using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using WebShop.Data.Models;

namespace WebShop.Data{
	public class WebShopContext : DbContext{

		// DbSet properties
		public DbSet<Product> Products{ get; set; }
		public DbSet<Manufacturer> Manufacturers{ get; set; }
		public DbSet<CategoryProduct> CategoryProducts{ get; set; }
		public DbSet<Category> Categories{ get; set; }
		public DbSet<Review> Reviews{ get; set; }
		public DbSet<Customer> Customers{ get; set; }

		public WebShopContext(DbContextOptions options) : base(options){ }

		public WebShopContext(){ }

		/**
		 * DatabaseContext options
		 */
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder){

			optionsBuilder.UseSqlServer("Server = (localdb)\\mssqllocaldb; Database = WebShopDb; Trusted_Connection = True; ") // I could use an in-memory database, but heck, I like to spend extra resources. Weird flex, but okay.
				.EnableSensitiveDataLogging(true)
				.UseLoggerFactory(new ServiceCollection() // We like logging because Ras likes logging.
					.AddLogging(builder => builder.AddConsole()
						.AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information))
					.BuildServiceProvider().GetService<ILoggerFactory>());
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder){
			// Seeding happens here at some point
			// Maybe also some fluid API
		}
	}
}