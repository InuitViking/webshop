﻿using System;
using System.Collections.Generic;
using WebShop.Data.Interfaces;

namespace WebShop.Service.Interfaces{
	public interface IRepository<TEntity> where TEntity : IEntity{
		void Create(TEntity entity);
		void Delete(TEntity entity);
		void Delete(int id);
		void Edit(TEntity entity);

		#region Read

		TEntity GetById(int id);
		IEnumerable<TEntity> Filter();
		IEnumerable<TEntity> Filter(Func<TEntity, bool> predicate);

		#endregion

		// Saving
		void SaveChanges();
	}
}