﻿using System;
using System.Collections.Generic;
using WebShop.Data.Interfaces;
using WebShop.Service.Interfaces;

// https://medium.com/@marekzyla95/generic-repository-pattern-implemented-in-net-core-with-ef-core-c7e088c9c58

namespace WebShop.Service{
	public class Repository<TEntity> : IRepository<TEntity>
		where TEntity : class, IEntity{
		public void Create(TEntity entity){
			throw new NotImplementedException();
		}

		public void Delete(TEntity entity){
			throw new NotImplementedException();
		}

		public void Delete(int id){
			throw new NotImplementedException();
		}

		public void Edit(TEntity entity){
			throw new NotImplementedException();
		}

		public TEntity GetById(int id){
			throw new NotImplementedException();
		}

		public IEnumerable<TEntity> Filter(){
			throw new NotImplementedException();
		}

		public IEnumerable<TEntity> Filter(Func<TEntity, bool> predicate){
			throw new NotImplementedException();
		}

		public void SaveChanges(){
			throw new NotImplementedException();
		}
	}
}